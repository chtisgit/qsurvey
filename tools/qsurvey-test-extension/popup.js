

chrome.runtime.onMessage.addListener(function (request, sender, sendResponse) {
	if (!request) {
		return;
	}
	if (request.error) {
		document.getElementById('errors').innerHTML += request.error+"<br>";
		return;
	}

	if (request.from == 'contentScript') {
		if (request.status){
			document.getElementById('status').innerHTML += request.status+"<br>";
			sendResponse({success: true});
			return;
		}
	}
});

function sendMsg(obj) {
	chrome.tabs.query({ active: true, currentWindow: true }, function (tabs) {
		obj.tab = tabs[0]
		obj.from = 'popup'
		chrome.tabs.sendMessage(obj.tab.id, obj)
	})
}

function test() {
	document.getElementById('testbutton').disabled = 'disabled';
	chrome.tabs.executeScript({
		file: 'contentScript.js'
	});
}

window.onload = function () {
	document.getElementById('testbutton').onclick = test; 
}
