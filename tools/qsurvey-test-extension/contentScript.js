
function isQSurvey() {
	var scripts = document.getElementsByTagName('script');
	for(var i = 0; i < scripts.length; i++){
		if(scripts[i].src.endsWith('websurvey.js')){
			return true;
		}
	}
	return false;
}

function rand(n){
	return Math.floor(Math.random()*n);
}

var report = [];

function showReport() {
	var p = document.createElement('p');
	p.className ="container";
	document.body.appendChild(p);

	p.innerHTML += '<h1>QSurvey Tester report</h1>'
	for(var i = 0; i < report.length; i++){
		p.innerHTML += "<b>"+report[i].id+"</b> selected: <em>"+report[i].value+"</em><br>";
	}
}

function selectRandom(div) {
	var inputs = div.getElementsByTagName('input');
	if (inputs.length === 0)
		return;
	switch(inputs[0].type){
	case 'text':
		report.push({
			id: div.id,
			type: 'text',
			value: 'test',
			selectionID: '',
		});
		inputs[0].value = 'test';
		break;
	case 'radio':
		var n = rand(inputs.length);
		report.push({
			id: div.id,
			type: 'radio',
			value: n+1,
			selected: [n+1],
		});
		inputs[n].click();
		break;
	case 'checkbox':
		var val = []
		for(var i = 0; i < inputs.length; i++){
			if(rand(2) === 1){
				inputs[i].click();
				val.push(i+1);
			}
		}
		report.push({
			id: div.id,
			type: 'checkbox',
			value: val,
			selected: val,
		});
		break;
	}
}

function testQuestion(n, retries, doneCb) {
	var div = document.getElementById('question'+n);
	if(!div){
		if(retries === 0){
			var buttons = document.getElementsByTagName('button');
			var button = buttons[buttons.length-1];
			if(button.type === 'button'){
				button.click();
			}
		}
		if(retries >= 5){
			doneCb();
			return;
		}
		window.setTimeout(function(){
			testQuestion(n, retries+1, doneCb);
		}, 500);
		return;
	}

	selectRandom(div);
	testQuestion(n+1, 0, doneCb);
}

function test() {
	testQuestion(1, 0, function(){
		showReport();
		chrome.runtime.sendMessage({
			status: 'done',
			from: 'contentScript',
		});
	});
}

if(isQSurvey()){
	chrome.runtime.sendMessage({
		status: 'running',
		from: 'contentScript',
	}, function (response) {
		if(response && response.success === true) {
			chrome.runtime.onMessage.addListener(function (request, sender, sendResponse) {
				console.log('got message');
				console.log(request);
			});
			test();
		}
	});
}else{
	chrome.runtime.sendMessage({
		error: 'no qsurvey found on the current page!',
	});
}

