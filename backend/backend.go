package backend

import "bitbucket.org/chtisgit/qsurvey/template"

// SurveyCreator is an interface for creating surveys from templates.
type SurveyCreator interface {
	Create(templ *template.T, path string) error
}
