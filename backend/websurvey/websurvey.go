package websurvey

import (
	"encoding/json"
	"errors"
	"io"
	"os"
	"strconv"

	htmlt "html/template"

	"bitbucket.org/chtisgit/qsurvey/template"
)

type T struct {
}

const staticfiles = "static/web"

func copy(from string, to string) error {
	newfile, err := os.Create(to)
	if err != nil {
		return err
	}
	defer newfile.Close()

	oldfile, err := os.Open(from)
	if err != nil {
		return err
	}
	defer oldfile.Close()

	_, err = io.Copy(newfile, oldfile)
	if err != nil {
		return err
	}
	return nil
}

type indexData struct {
	Title       string
	NavbarTitle string
	FormAction  string
	FormMethod  string
}
type beginData struct {
	SurveyID             string
	SurveyJSON           string
	SurveyTotalQuestions int
	Title                string
	Intro                []string
	StartButton          string
}
type answer struct {
	ID    int
	Text  string
	Value string
}
type questionData struct {
	ID         int
	Name       string
	Question   string
	NextButton string
	Answers    []answer
}
type endData struct {
	End          []string
	SubmitButton string
	AutoSubmit   bool
}

var defaultparameters = map[string]string{
	"StartButton":  "Start survey",
	"NextButton":   "Next",
	"SubmitButton": "Submit",
	"FormAction":   "submit.php",
	"FormMethod":   "POST",
	"NavbarTitle":  "QSurvey",
	"AutoSubmit":   "no",
}

func boolParam(p string) bool {
	if p == "no" {
		return false
	}
	return true
}

func createPage(path string, html *htmlt.Template, templatename string, data interface{}) error {
	file, err := os.Create(path)
	if err != nil {
		return err
	}
	defer file.Close()

	return html.ExecuteTemplate(file, templatename, data)
}

// Create a text file at the given path which contains
// the survey that is described in the template templ.
func (t T) Create(templ *template.T, path string) error {
	param := func(name string) string {
		val, found := templ.Backends["web"][name]
		if found {
			return val
		}
		val, found = defaultparameters[name]
		if found {
			return val
		}
		panic("no such parameter")
	}

	if path == "-" {
		return errors.New("specify an output directory")
	}
	err := os.Mkdir(path, 0755)
	if err != nil && !os.IsExist(err) {
		return err
	}
	err = os.Mkdir(path+"/page", 0755)
	if err != nil && !os.IsExist(err) {
		return err
	}

	files := []string{
		"websurvey.js",
		"style.css",
		"submit.php",
		"config.inc.php.sample",
	}

	for _, name := range files {
		copy(staticfiles+"/"+name, path+"/"+name)
	}

	html, err := htmlt.ParseFiles(staticfiles + "/template.html")
	if err != nil {
		return err
	}

	json, err := json.Marshal(templ.Questions)
	if err != nil {
		return err
	}

	if err := createPage(path+"/page/0.html", html, "begin", beginData{
		SurveyID:             templ.ID,
		SurveyJSON:           string(json),
		SurveyTotalQuestions: len(templ.Questions),
		Title:                templ.Title,
		Intro:                templ.Intro,
		StartButton:          param("StartButton"),
	}); err != nil {
		return err
	}

	page := 1
	for qnum, q := range templ.Questions {
		switch q.Type {
		case "singlechoice", "multiplechoice", "text":
			answers := []answer{}
			for anum, a := range q.Answers {
				answers = append(answers, answer{
					ID:    anum + 1,
					Text:  a,
					Value: strconv.Itoa(anum + 1),
				})
			}
			if err := createPage(path+"/page/"+strconv.Itoa(page)+".html", html, "question_"+q.Type, questionData{
				ID:         qnum + 1,
				Name:       q.Text,
				Question:   q.Text,
				NextButton: param("NextButton"),
				Answers:    answers,
			}); err != nil {
				return err
			}
			page++
		default:
			return errors.New("unsupported question type '" + q.Type + "'")
		}
	}

	if err := createPage(path+"/page/"+strconv.Itoa(page)+".html", html, "end", endData{
		End:          templ.End,
		SubmitButton: param("SubmitButton"),
		AutoSubmit:   boolParam(param("AutoSubmit")),
	}); err != nil {
		return err
	}

	if err := createPage(path+"/index.html", html, "indexhtml", indexData{
		Title:       templ.Title,
		NavbarTitle: param("NavbarTitle"),
		FormAction:  param("FormAction"),
		FormMethod:  param("FormMethod"),
	}); err != nil {
		return err
	}

	return nil
}
