package txtsurvey

import (
	"fmt"
	"os"

	"bitbucket.org/chtisgit/qsurvey/template"
)

type T struct {
}

// Create a text file at the given path which contains
// the survey that is described in the template templ.
func (t T) Create(templ *template.T, path string) error {
	out := os.Stdout
	if path != "-" {
		var err error
		out, err = os.Create(path + ".txt")
		if err != nil {
			return err
		}
		defer out.Close()
	}

	symbol := "< >"
	if templ.Backends["txt"]["symbol"] != "" {
		symbol = templ.Backends["txt"]["symbol"]
	}

	if templ.Title != "" {
		fmt.Fprintf(out, "%s\n", templ.Title)
		fmt.Fprintf(out, "==============================\n\n")
	}

	if templ.Author != "" {
		fmt.Fprintf(out, "From %s\n\n", templ.Author)
	}

	for _, par := range templ.Intro {
		fmt.Fprintf(out, "%s\n\n", par)
	}

	num := 1
	for _, q := range templ.Questions {
		fmt.Fprintf(out, "%3d. %s\n", num, q.Text)
		switch q.Type {
		case "singlechoice":
			fmt.Fprintf(out, "       (Choose only one of the answers)\n")
			fallthrough
		case "multiplechoice":
			for _, a := range q.Answers {
				fmt.Fprintf(out, "     %s %s\n", symbol, a)
			}
		case "text":
			fmt.Fprintf(out, "\n     ______________________________________________\n")
		}
		fmt.Fprintln(out, "")
		num++
	}

	for _, par := range templ.End {
		fmt.Fprintf(out, "%s\n\n", par)
	}

	return nil
}
