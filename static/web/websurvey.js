
var current_question = 0;
function survey_next_page() {
	var test = $('#question' + current_question + ' fieldset input[type=radio]');
	if (test.length != 0) {
		var i = 0;
		for (; i < test.length; i++) {
			if (test[i].checked)
				break;
		}
		if (i === test.length) {
			return;
		}
	}
	survey_loadpage(current_question+1);
}


function survey_renderpage(num, html){
	$('#question'+current_question).hide();
	var div = $('#question'+num);
	if(!div.length){
		div = $('<div>').attr('id','question'+num).css('display','none');
		div.appendTo($('#main'));
	}
	div.html(html);
	div.show();
	current_question = num;
}

function survey_loadpage(num) {
	$.get('page/'+num+'.html')
		.done(function (html) {
			survey_renderpage(num, html);
		})
		.fail(function (jqXHR, textStatus, errorThrown) {
			console.error('cannot get survey question '+num);
		})
}

function survey_submit() {
	var form = $('form');
	$('#submitbutton').hide();
	$.ajax({
		type: form.attr('method') || 'POST',
		url: form.attr('action'),
		data: form.serialize(),
	}).done(function(){
		console.log('form submitted');
	}).fail(function(){
		console.error('error submitting form');
	});
}

function survey_init() {
	if (!$ || !$.get) {
		console.error('no jquery');
		return;
	}

	survey_loadpage(0);
}