<?php

require('./config.inc.php');

function connectToDB()
{
	try {
		$db = new PDO(DB, DBUSER, DBPASS);
		$db->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
		initDB($db);
	} catch (PDOException $e) {
		die();
	}
	return $db;
}

function initDB($db)
{
	$r = $db->query('select id from survey_data limit 1');
	if($r !== false && $r->fetchColumn() !== false){
		$r->closeCursor();
		// db is initialized
		return;
	}

	try {
		if($db->exec(
			"create table if not exists survey_index (
			id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
			name VARCHAR(32) NOT NULL UNIQUE,
			questions INT NOT NULL,
			json TEXT NOT NULL,
			created DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP )"
		) === false) {
			die('error creating table');
		}
		if($db->exec(
			"create table if not exists survey_data (
			id INT NOT NULL,
			answersJSON TEXT NOT NULL,
			created DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
			answers INT NOT NULL )"
		) === false) {
			die('error creating table');
		}
	} catch (PDOException $e) {
		echo "error while initializing DB. $e";
	}
}

function insert($db, $name, $questions, $answers, $answersGiven)
{
	$survey = $db->prepare("select id from survey_index where name=?");
	if($survey === false) {
		die("could not save results 1 (prepare)");
	}
	if($survey->execute(array($name)) === false){
		die("could not save results 1 (execute)");
	}
	$id = $survey->fetchColumn();
	$survey->closeCursor();
	if($id === false){
		$survey = $db->prepare("insert into survey_index (name, questions, json) values (?, ?, ?)");
		if($survey === false) {
			die("could not save results 2 (prepare)");
		}
		if($survey->execute(array(
			$name,
			$questions,
			json_encode($_POST['surveyJSON'])
			)) === false) {
			die("could not save results (execute)");
		}
		$id = intval($db->lastInsertId());
	}

	$stm = $db->prepare("insert into survey_data (id,answersJSON,answers) values (?, ?, ?)");
	if($stm === false) {
		die("could not save results 3 (prepare)");
	}
	if($stm->execute(array(
		$id,
		json_encode($answers),
		$answersGiven
	)) === false){
		die("could not save results 4 (execute)");
	}
}

if(!isset($_POST['surveyID']) || ! $_POST['surveyID']){
	die("no survey ID");
}
$name = $_POST['surveyID'];
if(strlen($name) > 32){
	die();
}

if(!isset($_POST['surveyJSON']) || ! $_POST['surveyJSON']){
	die("no survey JSON");
}

if(!isset($_POST['surveyTotalQuestions']) || ! $_POST['surveyTotalQuestions']){
	die("no surveyTotalQuestions");
}
$questions = intval($_POST['surveyTotalQuestions']);
if($questions > 500){
	die();
}

$answers = array();

$answersGiven = 0;
for($q = 1; $q <= $questions; $q++){
	$answer = NULL;
	if(isset($_POST["useranswer$q"])){
		$answer = $_POST["useranswer$q"];
		$answersGiven++;
	}
	array_push($answers, $answer);
}

foreach($answers as &$a) {
	foreach($a as &$i) {
		if(preg_match('/^[0-9]+$/', $i)){
			$i = intval($i);
		}
	}
}

$db = connectToDB();
insert($db, $name, $questions, $answers, $answersGiven);


?>
