package template

import (
	"encoding/json"
	"os"
)

// Question holds the data of one question of the survey.
type Question struct {
	Type    string
	Text    string
	Answers []string
}

// T is the main template file type which contains the complete
// data structure of the template file.
type T struct {
	ID        string
	Title     string
	Author    string
	Intro     []string
	End       []string
	Questions []*Question
	Backends  map[string](map[string]string)
}

// Load template from path.
func Load(path string) *T {
	file, err := os.Open(path)
	if err != nil {
		return nil
	}
	defer file.Close()

	d := json.NewDecoder(file)

	templ := new(T)
	d.Decode(templ)
	return templ
}
