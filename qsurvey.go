package main

import (
	"flag"
	"fmt"
	"os"

	"bitbucket.org/chtisgit/qsurvey/backend"
	"bitbucket.org/chtisgit/qsurvey/backend/txtsurvey"
	"bitbucket.org/chtisgit/qsurvey/backend/websurvey"
	"bitbucket.org/chtisgit/qsurvey/template"
)

var templatefile = flag.String("template", "", "template file that is used to create the survey files")
var usebackend = flag.String("type", "txt", "Select the output type. Options: txt, web")
var output = flag.String("output", "-", "name of the output directory/file")

func main() {
	flag.Parse()
	//name := (*templatefile)[:strings.IndexRune(*templatefile, '.')]
	templ := template.Load(*templatefile)
	if templ == nil {
		fmt.Printf("error: please pass a valid template file\n\n")
		flag.Usage()
		os.Exit(1)
	}

	var b backend.SurveyCreator
	switch *usebackend {
	case "txt":
		b = txtsurvey.T{}
	case "web":
		b = websurvey.T{}
	default:
		fmt.Printf("error: no such output type '%s'\n\n", *usebackend)
		os.Exit(1)
	}
	if err := b.Create(templ, *output); err != nil {
		fmt.Println("An error occured while the survey was generated: ", err)
		fmt.Println("Aborting.")
	}
}
